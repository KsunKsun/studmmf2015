<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package SKT Full Width
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<?php /*<meta name="viewport" content="width=device-width, initial-scale=1">*/ ?>
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php 
$slAr = array();
for ($i=1;$i<6;$i++) {
	if ( of_get_option('slide'.$i, true) != "" ) {
		$imgUrl = of_get_option('slide'.$i, true);
		if ( strlen($imgUrl) > 3 ) $slAr[] = of_get_option('slide'.$i, true);
	}
}
?>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <!--<script type="text/javascript" src="wp-content/themes/skt-full-width/js/jquery.js"></script>-->
    <!--<script type="text/javascript" src="wp-content/themes/skt-full-width/js/custom.js"></script>-->

<?php
$front_page = get_option('page_on_front');
$post_page = get_option('page_for_posts');
?>
<div id="page" class="hfeed site">
	<?php do_action( 'before' ); ?>

	<?php if( (is_front_page() || is_home() ) && ($front_page == 0 && $post_page == 0) ){ ?>
		<?php if( count($slAr) > 0 ){ ?>
            <div class="slider-parent">	
                <div class="slider-wrapper theme-default container <?php if( is_front_page() || is_home()  ){ echo 'home_front_wrap_main'; } ?>"> 
                
                    

                    <div class="ball disable" id="ball"></div>
                    <a href="?page_id=31"><div class="calendar"></div></a>
                    <div class="furniture">
                        <div class="table">
                            <div class="zachetka"></div>
                        </div>
                        <div class="pc"></div>
                        
                    </div>
					<div class="avatar" id="avatar"></div>
                    <div class="stereo" id="stereo">
                        <div class="audiojs">
                            <div class="play-pause">
                                <p class="play"></p>
                                <p class="pause"></p>
                            </div>               
                        </div>
                    </div>

                </div><!--.container-->	
            </div><!--.slider-parent-->
            <script>
                var play=false;
                document.getElementById("stereo").onclick = function(){
                    
                    if(play){
                        play=false;
                        document.getElementById("ball").className = "ball disable";
                        document.getElementById("stereo").className = "stereo";
                        document.getElementById("avatar").className = "avatar";
                    }
                    else{
                        play=true;
                        document.getElementById("ball").className = "ball";
                        document.getElementById("stereo").className = "stereo speakerpulsate";
                        document.getElementById("avatar").className = "avatar dance"
                    }
                }
                
            </script>
        <?php } //if( count($slAr) > 0 ){ ?>
	<?php } ?>

    <div id="wrapper">
        <div id="secondary" class="widget-area <?php if( is_front_page() || is_home()  ){ echo 'home_front_wrap'; } ?>" role="complementary">
            <div class="header">
                <div class="logo">
                    <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                    	<?php if( of_get_option('logo', true) != '' ) { ?>
	                    	<img src="<?php echo esc_url( of_get_option('logo', true) ); ?>" />
                        <?php } else { ?>
							<?php bloginfo( 'name' ); ?>
                        <?php } ?>
                    </a></h1>
                    <h2 class="site-description"><?php bloginfo( 'description' ); ?></h2><br />
                </div>
              
                <div id="site-nav">
                    <h1 class="menu-toggle"><?php _e( 'Menu', 'skt-full-width' ); ?></h1>
                    <div class="screen-reader-text skip-link"><a href="#content"><?php _e( 'Skip to content', 'skt-full-width' ); ?></a></div>
                    <?php wp_nav_menu( array('theme_location' => 'primary', 'container' => '', 'menu_class' => '') ); ?>
                </div><!-- site-nav -->
                <div class="header-bottom">
                    <div id="header-bottom-shape">
                    </div><!-- header-bottom-shape2 -->
                </div><!-- header-bottom -->
                    
            </div><!-- header -->
        </div><!-- secondary -->
          