<?php 
/*
 * Template Name: Schedule*/
/**
 * The Template for displaying schedule page.
 *
 * @package SKT Full Width
 */

get_header();
?>


<ul id="schedule-bubbles" class="bubbles">

	<!-- First Course-->
	<li class="layer">
		<div class="bubble-black"></div>
	</li>

	<li class="layer">
		<div class="bubble-blue"></div>
	</li>

	<!--1-->
	<li class="layer">
		<a href="?page_id=33"><div class="bubble-blue-second bubble-menu"></div></a>
	</li>

	<!-- 2-->
	<li class="layer">
		<a href="?page_id=36"><div class="bubble-green bubble-menu"></div></a>
	</li>


	<li class="layer">
		<div class="bubble-orange"></div>
	</li>

	<!-- First Fourth-->
	<li class="layer">
		<a href="?page_id=38"><div class="bubble-orange-large bubble-menu"></div></a>
	</li>

	<!-- 5-->
	<li class="layer">
		<a href="?page_id=42"><div class="bubble-red bubble-menu"></div></a>
	</li>

	<li class="layer">
		<div class="bubble-red-second"></div>
	</li>

	<!--4-->
	<li class="layer">
		<a href="?page_id=40"><div class="bubble-violet-large bubble-menu"></div></a>
	</li>

	<li class="layer">
		<div class="bubble-violet"></div>
	</li>

</ul>
<script type="text/javascript" src="wp-content/themes/skt-full-width/js/custom.js"></script>

<?php wp_footer(); ?>
