��    a      $  �   ,      8     9  
   M     X     e     {     �     �  
   �  	   �  E   �  	   	     	  (   0	  
   Y	  �   d	     �	     
     "
     0
     5
     J
     W
     j
  ,   
     �
     �
     �
     �
     �
     	          '     :     P     ]     k     y          �     �     �     �     �  
   �     �  	   �     �     �          %     7  (   K     t     �     �     �     �     �     �     �     �  	   �     �  D        H     L     X     e     k  	   t  ]   ~     �  P   �  
   <     G  9   W     �     �     �  �   �  7   7  q   o  _   �     A     �     �  
   �     �     �     �  $        *     0     3     E     H  Z  d  '   �     �        2     (   @  2   i     �     �     �  d   �     R  '   c  .   �     �  t   �  8   E  -   ~     �  
   �  %   �     �  )     $   8  ;   ]  E   �  ^   �     >     K  ,   c  $   �     �  #   �  '   �          9     W     g     v  I   �     �  )   �          1     :     V     t     �  0   �  &   �  0   �  3   $  [   X      �     �     �     �  %        A     N     c     �     �     �  b   �     /     >  %   M  
   s     ~  +   �  �   �     y  �   �     T     l  @   y     �     �  
   �    �  (   �   �   &!  ^    "  �   _"     _#     r#     �#     �#  
   �#     �#  N   �#     $     $     $     .$     0$         !          L   2           S   .          <   F       P           %   E      U      Y   M   N      G   ]   C       +                '       ,   )      4       "   B       7           D       ?   ^      5   O   T   R   :      	       3   X               &       K      (      1   9              I   a          6   #          =          W             Q      
   /      J   \   A       V   8           `   @   _   >   H   *   ;                           $             0   -          Z   [     search results for % Comments %1$s at %2$s &larr; Older Comments &larr; Older posts &larr; Previous Image (edit) 0 Comments 1 Comment 1 comment for &ldquo;%2$s&rdquo; %1$s comments for &ldquo;%2$s&rdquo; 404 Error <i class="fa fa-link"></i> Link <i class="fa fa-plus-square"></i> Status Arc Radius Area on the home page above the main content. Designed specifically for four Icon & Text widgets. Add at least one widget to make it appear. Automatically add paragraphs Back to home &rarr; Blog Archives Blue Button & Icon Color: Button Text: Comment navigation Comments are closed. Copyright &copy; %s %s. All Rights Reserved. Custom Header Image Custom Text Widget with Icon Daily Archives: %s Default Default Header 1 Default Widget Display Author Display Categories Display Comment Count Display Date First Sidebar From the Blog Green Header Icon Home Page Top Area Icon: Jumbo Headline Layout Left Light Blue Main Content Width Main menu Monthly Archives: %s Newer Comments &rarr; Newer posts &rarr; Next Image &rarr; No posts to display No results were found. Please try again. Nothing found Page %s Pages: Post navigation Posted on %1$s at %2$s Posts Posts by %s Posts navigation Primary Menu Read more Read more &rarr; Ready to publish your first post? <a href="%s">Get started here</a>. Red Remove Icon Remove Image Right See More Set Image Set a custom image for the header if you want to use something other than the featured image. Sidebar Layout Sidebars do not appear on the home page unless you have set a static front page. Site Width Skip to content Sorry. We can't seem to find the page you're looking for. Tag Archive for %s Tags: Text The space and rotation for each letter will be calculated using the arc radius and the width of the site title. Leave blank for no arc. These options do not affect the home page post section. This is just a default widget. It'll disappear as soon as you add your own widgets on the %sWidgets admin page%s. This is the first sidebar. It won't appear on the home page unless you set a static front page. This section appears below the header image on the home page. To remove it just delete all the content from the Title textarea. Title Title: View Icons View more &rarr; Yearly Archives: %s Yellow Your comment is awaiting moderation. by %s in labelSearch for: on placeholderSearch &hellip; Project-Id-Version: Arcade Basic v10.0.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 
PO-Revision-Date: 2014-02-16 11:10+0200
Last-Translator: Игорь <dybabiz@ya.ru>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.6.2
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ../../
X-Textdomain-Support: yes
Language: ru
X-Poedit-SearchPath-0: .
  результаты поиска на % Комментарии %1$s в %2$s &larr; Предыдущие комментарии &larr; Предыдущие записи &larr; Предыдущее изображение (редактировать) 0 Комментариев 1 Комментарий 1 комментарий на &ldquo;%2$s&rdquo; %1$s комментарии на &ldquo;%2$s&rdquo; Ошибка 404 <i class="fa fa-link"></i> Ссылка <i class="fa fa-plus-square"></i> Статус Радиус дуги Виджет на главной в области приветствия для виджета: Icon & Text widgets Автоматически добавлять абзац Вернуться на главную &rarr; Архивы блога Синий Кнопка & Иконка Цвет: Текст кнопки: Комментарий навигация Обсуждение закрыто. Copyright &copy; %s %s. Все права защищены. Пользовательские изображение в шапке Отображает текст и иконками. Для виджета на главной День: %s По умолчанию По умолчанию Заголовок 1 По умолчанию виджет Показать Автора Показать Категории Показать Комментарии Показать Дату Боковая колонка Из блога Зеленый Заголовок Иконка Виджет на главной в области приветствия Иконка: Заголовок приветствия Расположение Лево Светло-голубой Основная Ширина Главное меню Месяц: %s Следующие комментарии &rarr; Следующие записи &rarr; Следующее изображение &rarr; Нет записей для отображения Ничего не найдено. Пожалуйста, попробуйте еще раз. Ничего не найдено Страница %s Страницы: Запись навигация Опубликовано %1$s в %2$s Записи Запись от %s Записи навигация Первичное меню Подробнее Подробнее &rarr; Готовы написать первую запись? <a href="%s">Начать здесь</a>. Красный Удалить Удалить изображение Право Больше Установить Изображение Установите пользовательское изображение для заголовка, если вы хотите использовать  другое изображение. Боковая колонка Боковые панели не появляются на главной странице, если вы не зададите статическую страницу на главной. Ширина сайта Наверх о данному адресу ничего не найдено. Метка: %s Метки: Текст Пространство и вращение для каждой буквы будет рассчитываться с использованием радиус дуги и ширины названия сайта. Чтобы отключить оставьте поле пустым. Параметры для записей Это просто виджет по умолчанию. Это исчезнет, как только вы добавить свои собственные виджеты на %sWidgets админ страницы%s. Этот виждет боковой панели на страницах с записями. Этот раздел появляется под изображением заголовка на главной странице. Чтобы удалить его просто удалить все содержимое из текстового поля Заголовок Заголовок: Выбор иконки Далее &rarr; Год: %s Желтый Спасибо! Ваш комментарий ожидает проверки.  %s - Поиск на: - Искать &hellip; 