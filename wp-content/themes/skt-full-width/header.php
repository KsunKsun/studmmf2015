<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package SKT Full Width
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<?php /*<meta name="viewport" content="width=device-width, initial-scale=1">*/ ?>
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php  
$slAr = array();
for ($i=1;$i<6;$i++) {
	if ( of_get_option('slide'.$i, true) != "" ) {
		$imgUrl = of_get_option('slide'.$i, true);
		if ( strlen($imgUrl) > 3 ) $slAr[] = of_get_option('slide'.$i, true);
	}
}
?>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php wp_enqueue_script('resize', get_template_directory_uri(). '/js/custom.js', array('jquery')); ?>


<?php
$front_page = get_option('page_on_front');
$post_page = get_option('page_for_posts');
?>
<div id="page" class="hfeed site">
	<?php do_action( 'before' ); ?>

	<?php if( (is_front_page() || is_home() ) && ($front_page == 0 && $post_page == 0) ){ ?>
		<?php if( count($slAr) > 0 ){ ?>
            <div class="slider-parent">	
                <div class="slider-wrapper theme-default container <?php if( is_front_page() || is_home()  ){ echo 'home_front_wrap_main'; } ?>"> 
                
                    <div id="loading" style="display:block;">
                    	<div class="wrapper-1">
							<div class="loader">
								<div class="cat"></div>
							</div>
							<div class="loader-text">Посмотри на котика, пока я загружаюсь</div>
						</div>
                    </div>

                    <div class="ball disable" id="ball"></div>

                    <a href="schedule"><div class="calendar" style="display:none;"></div></a>
                    <div class="furniture">
                        <div class="table" style="display:none;">
                            <div class="zachetka" style="display:none;"></div>
                        </div>
                        <div class="monitor" style="display:none;">
                            <div class="pc" style="display:none;"></div>
                        </div>
                        
                    </div>
					<div class="avatar" id="avatar" style="display:none;"></div>
                    <!--<object class="stereo" type="image/svg+xml" data=<?php echo get_template_directory_uri(). '/images/new_stereo.svg' ?>>sfdsfs</object>-->
                    <div class="stereo_wrap" style="display:none;">
	                    <div class="stereo"></div>
	                    <div class="stereo_anim"></div>
                    </div>

                </div><!--.container-->	
            </div><!--.slider-parent-->

        <?php } //if( count($slAr) > 0 ){ ?>
	<?php } ?>

    <div id="wrapper">
        <div id="secondary" class="widget-area <?php if( is_front_page() || is_home()  ){ echo 'home_front_wrap'; } ?>" role="complementary">
            <div class="header">
                <div class="logo">
                    <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                    	<?php if( of_get_option('logo', true) != '' ) { ?>
	                    	<img src="<?php echo esc_url( of_get_option('logo', true) ); ?>" />
                        <?php } else { ?>
							<span class="menu_header"><?php bloginfo( 'name' ); ?></span>
                        <?php } ?>
                    </a></h1>
                    <h2 class="site-description"><?php bloginfo( 'description' ); ?></h2><br />
                </div>
              
                <div id="site-nav">
                    <h1 class="menu-toggle"><?php _e( 'Menu', 'skt-full-width' ); ?></h1>
                    <div class="screen-reader-text skip-link"><a href="#content"><?php _e( 'Skip to content', 'skt-full-width' ); ?></a></div>
                    <?php wp_nav_menu( array('theme_location' => 'primary', 'container' => '', 'menu_class' => '') ); ?>
                </div><!-- site-nav -->
                <div class="header-bottom">
                    <div id="header-bottom-shape">
                    </div><!-- header-bottom-shape2 -->
                </div><!-- header-bottom -->
                    
            </div><!-- header -->
        </div><!-- secondary -->
          